import express from "express";
import Server from './server';
import morgan from 'morgan';
import './database'
import authRoutes from './routes/auth';
import dotenv from 'dotenv';
dotenv.config()

const port:number = 8000;
const server = Server.init(port);

server.app.use(express.json());
server.app.use(morgan('dev'));

server.app.use(authRoutes);

server.start(() => {
    console.log('Server ts-test runing in port', port)
});