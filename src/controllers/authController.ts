import {Request, Response} from 'express';
import User, {IUser} from '../models/User';
import jwt from 'jsonwebtoken';
import {validate} from '../libs/validations';

export const signup = async (req: Request,res:Response) => {
    validate(req, res);   
    const user:IUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });
    user.password = await user.ecryptPassword(user.password);
    const savedUser = await user.save();
    const token: string = jwt.sign({_id:savedUser._id},process.env.TOKEN_SECRET ||'1W5q6rhnxY4fAywpQdvgAs4KYzmsU629VnOMUnfXM3yhDqRHRqO26sjdIJBxpxHJ');
    res.header('token',token).json(savedUser);
};

export const signin = async (req: Request,res:Response) => {
    validate(req, res);
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).json('Email is wrong');
    const correctPassword:boolean = await user.validatePassword(req.body.password);
    if(!correctPassword) return res.status(400).json('Invalid Password');
    const token: string = jwt.sign({_id:user._id},process.env.TOKEN_SECRET ||'1W5q6rhnxY4fAywpQdvgAs4KYzmsU629VnOMUnfXM3yhDqRHRqO26sjdIJBxpxHJ',{
        expiresIn: 60*60*24
    });
    res.header('token',token).json(user);
};

export const restaurants = async (req: Request,res:Response) => {
    const user = await User.findById(req.userId);
    if (!user) return res.status(400).json('No user found');
    res.json(user);
};