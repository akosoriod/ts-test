import {Request, Response} from 'express';
import {validate} from '../libs/validations';
import fetch from 'node-fetch';


export const getRestaurants = async (req: Request,res:Response) => {
    validate(req, res);
    const [lat, long]=req.body.coordinates.split(",");
    const radius = 100;
    const types="restaurant";
    const url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lat+","+long+"&radius=" +radius+"&types=" + types + "&key="+ process.env.API_KEY_MAPS;
    const response = await fetch(url)
        .then(res =>  res.json())
        .catch(e => {
            console.error({
            'message':'error in get location method',
            error: e,
            });
        });

        res.json(response.results); 
};
