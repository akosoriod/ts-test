import express from "express";

export default class Server {
    public app: express.Application;
    public port: number;
    constructor(puerto:number){
        this.port = puerto;
        this.app = express();
    }
    static init (puerto: number) {
        return new Server (puerto);
    }
    get returnApp (){
        return this.app;
    }
    start(callback:() => void):void{
        this.app.listen( this.port, callback );
    }
}
