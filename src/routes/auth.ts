import {Router} from 'express';
import { verify } from 'jsonwebtoken';
import {signup, signin} from '../controllers/authController';
import {getRestaurants} from '../controllers/restaurantsController';
import {tokenValidation} from '../libs/verifyToken';
import {check, validationResult} from 'express-validator'
const router: Router = Router();


router.post('/signup', [
    check('username','Username must be have more than 3 characters')
    .exists()
    .isLength({min:3}),
    check('email','Email is not valid')
    .isEmail()
    .normalizeEmail(),
    check('password','Username must be have more than 3 characters')
    .exists()
    .isLength({min:3}),
    ],
    signup)

router.post('/signin',[
    check('email','Email is not valid')
    .isEmail()
    .normalizeEmail(),
    check('password','Pasword is required')
    .exists()
    ], 
    signin);

router.post('/restaurants',tokenValidation,[
    check('coordinates')
    .exists()
    .isLatLong()],
    getRestaurants);


export default router;