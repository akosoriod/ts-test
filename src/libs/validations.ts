import {Request, Response, NextFunction} from 'express';
import {check, validationResult} from 'express-validator'

export function validate(req: Request,res:Response) {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json(errors);
    } 
}