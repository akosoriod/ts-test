import {Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';

interface IPayload {
    _id: string;
    iat: number;
    exp: number;
}
export const tokenValidation = (req:Request,res: Response,next: NextFunction) => {
    const token =req.header('token');
    if (!token) return res.status(400).json('Access denied');
    const payload = jwt.verify(token, process.env.TOKEN_SECRET ||'1W5q6rhnxY4fAywpQdvgAs4KYzmsU629VnOMUnfXM3yhDqRHRqO26sjdIJBxpxHJ') as IPayload;
    req.userId = payload._id;
    next();
};