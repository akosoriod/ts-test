"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const authController_1 = require("../controllers/authController");
const restaurantsController_1 = require("../controllers/restaurantsController");
const verifyToken_1 = require("../libs/verifyToken");
const express_validator_1 = require("express-validator");
const router = express_1.Router();
router.post('/signup', [
    express_validator_1.check('username', 'Username must be have more than 3 characters')
        .exists()
        .isLength({ min: 3 }),
    express_validator_1.check('email', 'Email is not valid')
        .isEmail()
        .normalizeEmail(),
    express_validator_1.check('password', 'Username must be have more than 3 characters')
        .exists()
        .isLength({ min: 3 }),
], authController_1.signup);
router.post('/signin', [
    express_validator_1.check('email', 'Email is not valid')
        .isEmail()
        .normalizeEmail(),
    express_validator_1.check('password', 'Pasword is required')
        .exists()
], authController_1.signin);
router.post('/restaurants', verifyToken_1.tokenValidation, [
    express_validator_1.check('coordinates')
        .exists()
        .isLatLong()
], restaurantsController_1.getRestaurants);
exports.default = router;
//# sourceMappingURL=auth.js.map