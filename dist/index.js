"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server_1 = __importDefault(require("./server"));
const morgan_1 = __importDefault(require("morgan"));
require("./database");
const auth_1 = __importDefault(require("./routes/auth"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const port = 8000;
const server = server_1.default.init(port);
server.app.use(express_1.default.json());
server.app.use(morgan_1.default('dev'));
server.app.use(auth_1.default);
server.start(() => {
    console.log('Server ts-test runing in port', port);
});
//# sourceMappingURL=index.js.map